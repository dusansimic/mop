#!/bin/sh

# Copyright 2020 Dušan Simić <dusan.simic1810@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

[ "$(id -u)" -eq 0 ] && IS_SUDO=true
[ "$IS_SUDO" ] && INST_DIR=/opt || INST_DIR="$HOME/.local/opt"
[ "$IS_SUDO" ] && BIN_DIR=/usr/local/bin || BIN_DIR="$HOME/.local/bin"
[ "$IS_SUDO" ] && DESKTOP_DIR=/usr/share/applications || DESKTOP_DIR="$HOME/.local/share/applications"

[ "$IS_SUDO" ] && REPOS_DIR=/opt/mop/repos || REPOS_DIR="$HOME/.local/opt/mop/repos"

cd "$REPOS_DIR"

print_help() {
	echo "usage: mop <command> [arguments]"
	echo ""
	echo "package commands:"
	echo "    add       installs a package"
	echo "    remove    removes a package"
	echo "    info      prints info about a package"
	echo "    update    updates a package"
	echo ""
	echo "repo commands:"
	echo "    repo-add       adds a repo"
	echo "    repo-remove    removes a repo"
	echo "    repo-info      prints info about a repo"
	echo ""
	echo "help: mop help"
}

print_error() {
	echo "error: $1"
	exit 1
}

add() {
	pkgs=$(find . -type d | grep -v "/\.git" | grep "\.git" | grep -v "$1.*/" | grep "$1" | sed 's,\./,,')
	[ -z "$pkgs" ] && print_error "no packages found"
	cnt=1
	pkgsArr=()
	for pkg in $pkgs; do
		echo "$((cnt++))) $pkg"
		pkgsArr+=($pkg)
	done
	read pick
	cnt=1
	for pkg in $pkgs; do
		[ $cnt -eq $pick ] && break
		$((cnt++))
	done
	[ -z "$pkg" ] && print_error "no package selected"

	[ -f "$pkg/build" ] || print_error "build file not found"
	cd "$pkg"
	sh "./build" "$INST_DIR" "$BIN_DIR" "$DESKTOP_DIR"
	echo "done"
}

remove() {
	[ -d "$1" ] || echo "package not found"
	[ -f "$1/remove" ] || echo "remove file not found"
	sh "$1/remove" "$INST_DIR" "$BIN_DIR" "$DESKTOP_DIR"
	echo "done"
}

info() {
	[ -d "$1" ] || echo "package not found"
	[ -f "$1/info" ] | echo "no package info found"
	echo "[$1]"
	cat "$1/info"
}

update() {
	REPOS_LIST="$(find . -name .git -type d -prune)"
	for R in $REPOS_LIST; do
		cd "$REPOS_DIR/$R/.."
		git pull
	done
}

repo_add() {
	[ "$(command -v git)" ] || print_error "command git not found"
	[ -z "$1" ] && print_error "repo was not specified"
	REPO_NAME="$(echo "$1" | sed -re 's-.*(@|://)(.*)(:|/)(.*)(\.git)?$-\2/\4-')"
	git clone "$1" "$REPO_NAME"
	echo "added repo $REPO_NAME"
}

repo_remove() {
	[ -d "$1" ] || print_error "repo not found"
	rm -rf "$1"
	find . -type d -empty -delete
	echo "repo $1 removed"
}

repo_info() {
	[ -d "$1" ] || print_error "repo not found"
	[ -f "$1/info" ] || print_error "no repo info found"
	echo "[$1]"
	cat "$1/info"
}

if [ "$1" = "help" -o "$1" = "h" ]; then
	print_help
elif [ "$1" = "add" -o "$1" = "a" ]; then
	add "$2"
elif [ "$1" = "remove" -o "$1" = "r" ]; then
	remove "$2"
elif [ "$1" = "info" -o "$1" = "i" ]; then
	info "$2"
elif [ "$1" = "update" -o "$1" = "u" ]; then
	update
elif [ "$1" = "repo-add" -o "$1" = "ra" ]; then
	repo_add "$2"
elif [ "$1" = "repo-remove" -o "$1" = "rr" ]; then
	repo_remove "$2"
elif [ "$1" = "repo-info" -o "$1" = "ri" ]; then
	repo_info "$2"
else
	print_help
fi
