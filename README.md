# Minimal Opinionated POSIX compliant Package Manager

> Mop is a minimal package manager for installing packages via simple
> scripts.

## Idea

I wanted to write scripts for installing and uninstalling packages that weren't
available in default distro repositories. I could've used Flatpak or AppImage
when available however they use a large amount of disk space since they include
all dependencies in the package. The idea was to use a POSIX compatible shell
scripts to install the package from tar balls or other sources and setup path
and desktop files. Since I wanted to practice writing POSIX scripts, I decided
to create a very minimal package manager that will hanlde package installation.
In that sense, it's very similar to how Arch PKGBUILD works.

## Usage

It's expected of you to have the required directory structure ready. The following dirs are
expected.

* `$HOME/.local/opt` - for installing packages
* `$HOME/.local/bin` - for installing binaries
* `$HOME/.local/opt/mop/repos` - for repos

Or if running as sudo:

* `/opt`
* `/usr/local/bin`
* `/opt/mop/repos`

respecively. You can change the dirs you want to use in the script itself.

For commands run

```sh
$ mop help
```

## Copyright

MIT &copy; Dušan Simić <<dusan.simic1810@gmail.com>> 2020
